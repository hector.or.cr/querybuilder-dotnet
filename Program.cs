﻿using System;
using Microsoft.Data.Sqlite;
using System.Data;
using System.Reflection;

namespace Querybuilder
{
    internal class Program
    {
        static void Main(string[] args)
        {
        
		  var query = (new QueryBuilder());

		  //insertar_usuarios(query);
		  //insertar_roles(query);


		/* QueryBuilder.select() */
		/*
		   query
		   .from("roles")
		   .select("id", "name");
		*/
		
		/*---------------------------------------------------------*/
		/* QueryBuilder.distinct() */
		/* SELECT DISTINCT * FROM role_permission */
		/*---------------------------------------------------------*/
		// query
		// .from("role_permission")
		// .distinct();


		/*---------------------------------------------------------*/
		/* QueryBuilder.from() */
		/* SELECT * FROM role_permission AS permisos, roles WHERE permisos.role_id = roles.id */
		/*---------------------------------------------------------*/
		//  query
		//  .from("role_permission", "permisos")
		//  .from("roles")
		//  .where("permisos.role_id = roles.id");


		/*---------------------------------------------------------*/
		/* QueryBuilder.join() */
		/* SELECT roles.*, permissions.name AS permission FROM role_permission 
		/* INNER JOIN roles ON role_permission.role_id = roles.id  INNER JOIN permissions ON role_permission.permission_id = permissions.id */
		/*---------------------------------------------------------*/
		//  query
		//  .from("role_permission")
		//  .join("roles", "role_permission.role_id = roles.id", "inner")
		//  .join("permissions", "role_permission.permission_id = permissions.id", "inner")
		//  .select("roles.*", "permissions.name AS permission");

		
		/*---------------------------------------------------------*/
		/* QueryBuilder.subQuery() */
		/* SELECT users.* FROM users, (SELECT * FROM role_user WHERE role_id < 10) AS roles WHERE roles.user_id = users.id */
		/*---------------------------------------------------------*/
// 		 var subquery = (new QueryBuilder())
// 		 .from("role_user")
// 		 .where("role_id < 10");
// // 
// 		 query
// 		 .from("users")
// 		 .select("users.*")
// 		 .subQuery(subquery, "roles")
// 		 .where("roles.user_id = users.id");

		

		/*---------------------------------------------------------*/
		/* QueryBuilder.where() */
		/* SELECT * FROM roles WHERE active = 1
		/*---------------------------------------------------------*/
		 query
		 .from("roles")
		 .where("active = 1", $"name = 'Medical Assistant'");


		/*---------------------------------------------------------*/
		/* QueryBuilder.orWhere() */
		/* SELECT * FROM permissions WHERE active = 1 OR id < 10
		/*---------------------------------------------------------*/
		//  query
		//  .from("permissions")
		//  .where("active = 1")
		//  .orWhere("id < 10");


		/*---------------------------------------------------------*/
		/* QueryBuilder.orWhere() */
		/* SELECT id, name AS nombre FROM permissions WHERE active = 1 AND id IN (SELECT permission_id FROM role_permission WHERE role_id IN (1, 10))
		/*---------------------------------------------------------*/
		// var subquery = (new QueryBuilder())
		// .select("permission_id")
		// .from("role_permission")
		// .whereIn("role_id", "1", "10");

		//  query
		//  .from("permissions")
		//  .select("id", "name AS nombre")
		//  .where("active = 1")
		//  .whereIn("id", subquery);


		/*---------------------------------------------------------*/
		/* QueryBuilder.paginate() */
		/* SELECT * FROM permissions LIMIT 10 OFFSET 30
		/*---------------------------------------------------------*/
		//   query
		//   .from("permissions")
		//   .paginate(4, 10);

		
		/*---------------------------------------------------------*/
		/* QueryBuilder.update() */
		/* UPDATE users SET surname=roles.name FROM roles WHERE users.name LIKE '%hector' AND roles.id = users.id
		/*---------------------------------------------------------*/
		//   query
		//   .table("users")
		//   .from("roles")
		//   .update(new Dictionary<string, string>(){
		// 	{"surname", "roles.name"},
		// 	})
		//   .like("users.name", "hector", "before")
		//   .where("roles.id = users.id");


		/*---------------------------------------------------------*/
		/* QueryBuilder.update() */
		/* UPDATE users SET surname=roles.name FROM roles INNER JOIN role_permission ON roles.id = role_permission.role_id  WHERE roles.id = users.id AND role_permission.baja = 0
		/*---------------------------------------------------------*/
		// query
		// .table("users")
		// .from("roles")
		// .join("role_permission", "roles.id = role_permission.role_id", "inner")
		// .update(new Dictionary<string, string>(){
		// 	{"surname", "roles.name"},
		// 	})
		// .where("roles.id = users.id")
		// .where("role_permission.baja = 0");


		/*---------------------------------------------------------*/
		/* QueryBuilder.update() */
		/* UPDATE users SET surname=roles.name FROM (SELECT name, id FROM roles WHERE baja = 0) AS roles WHERE users.name LIKE '%hector' AND roles.id = users.id
		/*---------------------------------------------------------*/

		/*var subquery = (new QueryBuilder())
		.select("name", "id")
		.from("roles")
		.where("baja = 0");*/

		
		  /*query
		  .table("users")
		  //.subQuery(subquery, "roles")
		  .update(new Dictionary<string, string>(){
			{"surname", "roles.name"},
			})
		  .like("users.name", "hector", "before")
		  .where("roles.id = users.id");*/


		/*---------------------------------------------------------*/
		/* QueryBuilder.delete() */
		/* DELETE FROM role_permission WHERE baja = 0
		/*---------------------------------------------------------*/
		//   query
		//   .table("role_permission")		  
		//   .where("baja = 0")
		//   .delete();		

		
		/**MethodInfo[] methods = typeof(QueryBuilder).GetMethods();
		var queryn = new QueryBuilder();
		 foreach (MethodInfo methodInfo in methods)
            {
                Console.WriteLine(methodInfo.Name + "<br/>");
            }
		MethodInfo[] methodsof = queryn.GetType().GetMethods();

		foreach (MethodInfo methodInfo in methodsof)
            {
                Console.WriteLine(methodInfo.Name + "\n");
            }
			**/

			 //Console.WriteLine(query.ToString());
			 DateTime ingreso = new DateTime(2020, 8, 24);
			 Console.WriteLine(ingreso.ToString());
			 Console.WriteLine(DateTime.Today);
			 Console.WriteLine((DateTime.Today - ingreso).TotalDays / 365.25);
        }


		static void insertar_roles(QueryBuilder query)
		{
			Role[] roles = new Role[]{
				new Role(1, "'Administrator'", 1),
				new Role(2, "'Moderator'", 1),
				new Role(3, "'Visitor'", 1),
				new Role(4, "'Lecturer'", 1)
			};

			foreach (Role role in roles){
				query.insert(role);
			}
			query.table("roles").columns("name", "active");
			ExecuteReader(query);
		}
	
		static void insertar_usuarios(QueryBuilder query)
		{
			Dictionary<string, string>[] users = new Dictionary<string, string>[] 
	      	{
	      		new Dictionary<string, string>()
				{ 
					{"name", "'hector'"},
					{"surname", "'crispens'"},
					{"active", "1"},
					{"edad", "42"}
				},
				new Dictionary<string, string>()
				{
					{"name", "'natalia'"},
					{"surname", "'rodriguez'"},
					{"active", "1"},
					{"edad", "41"}
				}
			};

			Dictionary<string, string> user = new Dictionary<string, string>()
				{
					{"name", "'juan domingo'"},
					{"surname", "'gutierres'"},
					{"active", "1"},
					{"email", "test@email.com"}
				};

			query.table("users")
				.columns("name", "surname")
				.insert(users)
				.insert(user);
			
			ExecuteReader(query);

		}

		private static void ExecuteReader(QueryBuilder query)
		{

			using (var connection = new SqliteConnection("Data Source=database.db"))
            {
				connection.Open();
                var command = connection.CreateCommand();

				command.CommandText = query.ToString();

                using (var reader = command.ExecuteReader())
                {
                    for(int i=0; i<reader.FieldCount; i++)
        				Console.Write($"{reader.GetName(i)}\t");
					Console.WriteLine("\n");
                    while (reader.Read())
					{
						//Console.WriteLine("{0}\t{1}\t{2}", reader.GetInt32(0),
						//reader.GetString(1), reader.GetString(2));
						//Console.WriteLine("\n");

						Object[] values = new Object[reader.FieldCount];
    					int fieldCount = reader.GetValues(values);

						Console.WriteLine(String.Join("\t", values));
					}
					

					
                    
                }
			}

		}

		private static void ShowTable(DataTable table)
    {
        foreach (DataColumn col in table.Columns)
        {
            Console.Write("{0,-14}", col.ColumnName);
        }
        Console.WriteLine();

        foreach (DataRow row in table.Rows)
        {
            foreach (DataColumn col in table.Columns)
            {
                if (col.DataType.Equals(typeof(DateTime)))
                    Console.Write("{0,-14:d}", row[col]);
                else if (col.DataType.Equals(typeof(Decimal)))
                    Console.Write("{0,-14:C}", row[col]);
                else
                    Console.Write("{0,-14}", row[col]);
            }
            Console.WriteLine();
        }
        Console.WriteLine();
    }

	
    }
}
