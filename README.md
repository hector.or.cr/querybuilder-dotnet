

![Images/logo.png](./Images/logo.png)


# QueryBuilder-dotnet

[![labels](https://img.shields.io/badge/Core-6.0.404-512BD4?style=plastic&logo=.net)](https://dotnet.microsoft.com/es-es/) [![labels](https://img.shields.io/badge/SQLite-3.41.2-003B57?style=plastic&logo=sqlite)](https://dotnet.microsoft.com/es-es/) [![labels](https://img.shields.io/badge/MySQL-8.0-4479A1?style=plastic&logo=mysql)](https://dotnet.microsoft.com/es-es/) [![labels](https://img.shields.io/badge/PostgreSQL-15.2-4169E1?style=plastic&logo=postgresql)](https://dotnet.microsoft.com/es-es/) 



## Contributors

[![hector](https://raw.githubusercontent.com/hectorcrispens/dockAngl/master/img/avatar-hector.svg)](https://www.linkedin.com/in/hector-orlando-25124a18a/)  


## Tabla de contennido
[TOC]




## Query Builder Class

**Query Builder** es una clase que permite construir sentencias **sql** de una  forma mucho más sencilla y rápida a través de la implementación de  distintos métodos.
También tiene la ventaja  que libera al usuario de la sintaxis y asegura como resultado final una sentencia **sql** bien conformada.

A través del retorno de la misma entidad  se pueden encadenar distintas condiciones o características de manera tal que es mucho mas sencilla  su utilización y a la vez lleva un control de las partes del de la sentencia con lo cual no es necesario respetar un orden ni llevar un control estricto.

Query builder se situa en una capa superior a las declarativas sentencias **raw sql** pero por debajo del patrón **active record** o el patrón **DAO**  pero a la vez con la suficiente flexibilidad para encadenar sentencias, armar sub-querys, etc.

El **QueryBuilder** admite cuatro operaciones, y siempre intentará ejecutar alguna de estas operaciones, esas operaciones son `[seleccion]`, `[inserción]`, `[actualización]` o `eliminación]`. Es decir que según los métodos que se hayan invocados, intentará formatear estos datos a una query conveniente segun estos cuatro tipos. Si por ejemplo se ejecutan métodos de **selección** intentará armar una query de **selección**, pero si por ejemplo se ejecutan métodos de selección y posteriormente se ejecutan métodos de **inserción**,  el QueryBuilder cambiará su modo a **inserción** e intentará formatear los datos como una query de **inserción**.

## Indice y clasificación de métodos

**QueryBuilder** se se divide en varios grupos de métodos que tienen diferentes propósitos, estos grupos de métodos están separados según su función en cada uno de los archivos que componen la clase **QueryBuilder**. Con lo cual es posible incorporarlos según su necesidad a la compilación total de la clase.  Ejemplo, los métodos de actualización de datos se hayan dentro del archivo `QueryBuilder.update.cs` mientras que los de selección se encuentran en el archivo `QueryBuilder.select.cs` mientras que los que son de uso global al igual que los parámetros se hayan en el archivo principal `QueryBuilder.cs`

### Indice y agrupamiento de los métodos

A continuación vamos a definir el indice de archivos, los grupos de métodos, etc.

#### QueryBuilder.cs

- RESETEOS
  1. QueryBuilder.reset()
- TABLAS
  1. QueryBuilder.table()
- ORIGENES DE DATOS
  1. QueryBuilder.from()
  2. QueryBuilder.join()
  3. QueryBuilder.subQuery()
- CONDICIONES
  1. QueryBuilder.where()
  2. QueryBuilder.whereIn()
  3. QueryBuilder.whereNotIn()
  4. QueryBuilder.like()
  5. QueryBuilder.orWhere()
  6. QueryBuilder.orWhereIn()
  7. QueryBuilder.orWhereNotIn()
  8. QueryBuilder.orLike()
  9. QueryBuilder.orNotLike()
  10. QueryBuilder.groupStart()
  11. QueryBuilder.orGroupStart()
  12. QueryBuilder.groupEnd()
- OPERACIONES DE CONJUNTO
  1. QueryBuilder.union()






#### QueryBuilder.select.cs

- SELECCION
  1. QueryBuilder.select()
  2. QueryBuilder.distinct()
- PAGINACION
  1. QueryBuilder.paginate()
- ORDENACION
  1. QueryBuilder.orderBy()
- AGRUPAMIENTO
  1. QueryBuilder.groupBy()



#### QueryBuilder.insert.cs

- INSERCION
  1. QueryBuilder.columns()
  2. QueryBuilder.insert()



#### QueryBuilder.update.cs

- ACTUALIZACION
  1. QueryBuilder.update()



#### QueryBuilder.delete.cs

- ELIMINACION
  1. QueryBuilder.delete()



## Ejemplo práctico

Para ejemplificar la aplicación hemos creado un proyecto de consola. Una recomendación práctica es trabajar con contenedores de **docker**, los cuales son una herramienta que simplifican el trabajo ya que permite empaquetar aplicaciones sin dejar residuos en el sistema operativo. Para obtener mas información de **docker**  puede abrir el [link](https://www.docker.com/):

Para este ejemplo se utilizó un contenedor creado específicamente con el **SDK** de **.NetCore**. Para utilizarlo puede descargarlo [aquí](https://github.com/hectorcrispens/dockDotNet)

Para instalar el paquete de **SQLite**  que se utiliza en el ejemplo se debe ejecuta el siguiente `command`.

```sh
dotnet add package Microsoft.EntityFrameworkCore.Sqlite
```

En este ejemplo vamos a suponer que tenemos 3 *(tres)* tablas principales, una tabla `users`, una tabla `roles` y una tabla `permissions`. Esto nos permite crear el patrón **RBAC**, es decir un sistema de permisos para una aplicación. La relación entre estas tres tablas es una relación `many-to-many` lo que nos permite hacer pruebas completas de nuestro **QueryBuilder**.

#### Diagrama de tablas

```mermaid
---
title: Patrón RBAC
---
erDiagram
   USERS {
        int id
        string name
        string surname
    }
    ROLES {
    	int id
    	string name
    	int active
    }
    PERMISSIONS {
    	int id
        string name
        int active
    }
    ROLE_USER {
    	int user_id
    	int role_id
    	int baja 
    }
    ROLE_PERMISSION {
    	int role_id
    	int permission_id
    	int baja
    }
    
    USERS ||--|{ ROLE_USER : users
    ROLES ||--|{ ROLE_USER : roles
    PERMISSIONS ||--|{ ROLE_PERMISSION : permissions
    ROLES ||--|{ ROLE_PERMISSION : roles
```





## Creación de instancia

La siguiente instrucción permite la creación de una instancia. Para utilizar **query builder** es necesario crear una instancia de la clase  y por otro lado es importante que para generar cualquier consulta de datos se debe invocar la función `from()` al menos una vez.

```c#
var query = (new QueryBuilder())
		.from("empleados");

Console.Writeline(query);
/*
* Produce el siguiente resultado:
* SELECT * FROM empleados 
*/
```

El resultado que arroja 
Cómo se puede observar cada uno de los métodos retorna nuevamente la misma instancia de **QueryBuilder**. Esto tiene la ventaja de que se pueden encaderar las funciones.

## Inserción de datos

Siguiendo con el ejemplo del patrón RBAC, nos propondremos poblar las tablas utilizando el **QueryBuilder**. A continuación se describen todas las operaciones para la inserción de datos como también la máquina de estados que genera las queries de inserción.

![insert](./Images/insert_state.png)



### Tablas

#### QueryBuilder.table()

El método `table()` se utiliza tanto para las operaciones de `[inserción]`, `[actualización]` y `[eliminación]` no así para las de `[selección]`. Siempre que se va a hacer alguna de estas operaciones se debe definir en que tabla se va a trabajar.

```c#
var query = (new QueryBuilder())
		.table("users");

/*
* Esto no produce resultados:
* Pero define la tabla en la cual se hará inserción, actualización o eliminación de datos
*/
```



### Campos

#### QueryBuilder.columns()

Si bien muchos objetos pueden tener gran cantidad de propiedades, este método define que propiedades se tendrán en cuenta y cuales serán ignoradas. Por ejemplo, un objeto o un diccionario de datos pueden contener las claves *name*, *surname, edad y dni*, sin embargo nuestra tabla solo tiene los campos *name* y *surname* con la función `columns()` le estamos indicando que de esos objetos lo único que vamos a considerar son los campos *name y surname* y que dni y edade serán  ignorados.

```c#
var data = new Dictionary<string, string>()
			{ 
				{"name", "'elias'"},
				{"surname", "'gregorutti'"},
				{"edad", "42"},
    			{"dni", "99999999"}
			}
var query = (new QueryBuilder())
		.table("users")
		.columns("name", "surname")
    	.insert(data);
    
	Console.WriteLine(query);
/*
* Produce el siguiente resultado:
* INSERT INTO users (name, surname) VALUES ('elias', 'gregorutti');
*/
```



### Datos

QueryBuilder.insert()

El método `insert()` tiene sobrecarga, es decir que puede recibir distintos tipos de parámetros según la necesidad. Por ejemplo se puede invocar pasándole como parámetro un array de `Dictionary` es decir un lote de datos completo como para poblar una  tabla.
También se le puede pasar como parámetro un único `Dictionary` o bien se puede pasar un objeto de cualquier tipo siempre y cuando las  propiedades del objeto coincidan con las columnas de la tabla.
Es importante  destacar que QueryBuilder va acumulando los datos o los lotes de datos en la medida que se invoca el método `insert()` con lo cual es posible encadenar consultas y al momento retornar construirá la query con todos los datos cargados en el dataset de la clase `QueryBuilder`.

```c#
/* A continuación se ilustra todas las posibles combinaciones y sobrecargas en el metodo insert()
* Suponemos que la definición de la clase Role es la siguiente
* 	class Role{
*		int id {get; set;}
*		string name {get; set;},
*		int active {get;set;}
* }
*/

Dictionary<string, string> role_by_dic = new Dictionary<string, string>()
			{ 
				{"name", "'administrador'"},
				{"active", "1"}
			}
 Dictionary<string, string>[] roles = new Dictionary<string, string>[] 
	            {
                    new Dictionary<string, string>()
                        { 
                            {"name", "'avanzado'"},
                            {"active", "1"}
                        },
                    new Dictionary<string, string>()
                        {
                            {"name", "'consulta'"},
                            {"active", "1"}
                        }
                };
Role obj_rol = new Role() {active=4, name="'supervisor'"};

var query = (new QueryBuilder())
		.table("roles")
		.columns("name", "id")
    	.insert(role_by_dic)
    	.insert(roles)
    	.insert(obj_rol);
    
	Console.WriteLine(query);
/*
* Produce el siguiente resultado:
* INSERT INTO roles (name, id) VALUES ('administrador', 1), ('avanzado', 2), ('consulta', 3), ('supervisor' , 4);
*/
```



## Eliminación de datos 

La eliminación de datos se realiza mediante el único método de eliminación `delete()`. Es decir que para la eliminación de datos necesitamos, dos métodos obligados: el de eliminación  `delete()` y el de tabla `table()` y luego de manera opcional los métodos de `[Condiciones]`.

A continuación se ilustra la maquina de estados de las consultas **Delete**

![delete](./Images/delete_state.png)

#### QueryBuilder.delete()

El método de eliminar permite la eliminación de datos de una tabla, éste método se puede invocar utilizando `[Condiciones]`  o bien  prescindiendo de ellos en cuyo caso se eliminarán todos los datos de la  tabla el método no recibe parámetros invocación configura el **QueryBuilder** para devolver una sentencia del tipo delete.

```c#
 var   query = (new QueryBuilder())
		 .table("role_permission")		  
		  .where("baja = 0")
		  .delete();		
		  
Console.WriteLine(query);
/*
* Produce el siguiente resultado:
* DELETE FROM role_permission WHERE baja = 0
*/
```



## Actualización de datos

La actualización de datos se basa en métodos que ya vimos, como lo es el método `QueryBuider.table()` o todos los métodos de **condiciones** del apartado de **SELECCIÓN DE DATOS**, es decir los métodos `QueryBuilder.where()`, `QueryBuilder.whereIn()`, `QueryBuilder.like()`, etc... y los métodos de orígenes de datos como `QueryBuilder.from()` o `QueryBuilder.subQuery()`

A continuación se describe la máquina de estados que construye la query **UPDATE**.

![update](./Images/update_state.png)



### Tablas

#### QueryBuilder.table()

El método `table()` define la tabla sobre la cual se hará la actualización de datos. Toda operación de actualización requiere la definición de una tabla con lo cual, siempre deberá ser invocado este método en operaciones de actualización.

```c#
var data = new Dictionary<string, string>()
		{ 
			{"name", "'hector'"}
		}
var query = (new QueryBuilder())
		.table("users")
		.update(data);
		
Console.WriteLine(query);
/*
* Produce el siguiente resultado:
* UPDATE users SET name='hector'
*/
```



### Orígenes de datos

#### QueryBuilder.from()

En el ejemplo anterior se mostro como es posible modificar una columna de una tabla pasando como parámetro el nuevo valor que tomarán todas celdas de dicha columna. Sin embargo, es posible obtener ese valor desde otra tabla. La función `from()` en el contexto de actualización nos permite seleccionar datos de otra tabla para luego actualizar la tabla que queremos. 

**¡Aclaración importante!**

Es importante aclarar, que las consultas de actualización que contienen como origen de datos a otros conjuntos de datos, es decir otras tablas o tablas intermedias producto de subconsultas, se deben incluir con **condiciones**. Es decir, la consulta final debe tener la siguiente estructura: `UPDATE [table] SET [column=expression], ... FROM [sources...] WHERE [condiciones...]`.

Veamos un ejemplo.

```c#
var query = (new QueryBuilder())
	.table("users")
	.from("roles")
	.update(new Dictionary<string, string>(){
		{"surname", "roles.name"},
		})
	.like("users.name", "hector", "before")
	.where("roles.id = users.id");
		
Console.WriteLine(query);
/*
* Produce el siguiente resultado:
* UPDATE users SET surname=roles.name FROM roles WHERE users.name LIKE '%hector' AND roles.id = users.id
*/
```



#### QueryBuilder.join()

Dentro de las consultas de actualización que incluyen origenes de datos tenemos las  que incluyen un unico origen o tabla de referencia. O tambien podemos tomar como origen un conjunto mas grande de datos a partir de juntar dos o mas tablas. Recuerde que la operación `join` se puede incluir **n** número de veces.

```c#
var query = (new QueryBuilder())
	.table("users")
	.from("roles")
	.join("role_permission", "roles.id = role_permission.role_id", "inner")
	.update(new Dictionary<string, string>(){
		{"surname", "roles.name"},
		})
	.where("roles.id = users.id")
	.where("role_permission.baja = 0");
		
Console.WriteLine(query);
/*
* Produce el siguiente resultado:
* UPDATE users SET surname=roles.name FROM roles INNER JOIN role_permission ON roles.id = role_permission.role_id  WHERE roles.id = users.id AND role_permission.baja = 0
*/
```



#### QueryBuilder.subQuery()

La función de `subQuery()` se puede ver en mayor detalle dentro de las operaciones de **SELECCIÓN**. Pero su función principal consiste en incorporar como `[origen de datos]` los resultados de una consulta de datos secundaria.

```c#
var subquery = (new QueryBuilder())
		.select("name", "id")
		.from("roles")
		.where("baja = 0");

		
var query = (new QueryBuilder())
		  .table("users")
		  .subQuery(subquery, "roles")
		  .update(new Dictionary<string, string>(){
			{"surname", "roles.name"},
			})
		  .like("users.name", "hector", "before")
		  .where("roles.id = users.id");

Console.WriteLine(query);
/*
* Produce el siguiente resultado:
* UPDATE users SET surname=roles.name FROM (SELECT name, id FROM roles WHERE baja = 0) AS roles WHERE users.name LIKE '%hector' AND roles.id = users.id
*/
```



### Condiciones

#### QueryBuilder.where(), QueryBuilder.whereIn(), QueryBuilder.like(), QueryBuilder.groupStart(), ....

Durante esta sección se estuvieron mostrando como las funciones de orígenes de datos requieren la invocación a funciones de condiciones con lo cual en este apartados no se detallarán en profundidad. Para mayor detalle consulte la sección de selección de datos.



### Actualización

#### QueryBuilder.update()

Actualización el método permite definir los campos que serán  actualizados, estos valores deben ser pasados como parámetros del tipo par `key => value`. esta asignación de valores puede hacerse a  través de un `Dictionary<string, string>` o mediante una instancia de  `object` es decir que se puede pasar cualquier implementación de clase.
Es importante destacar que dentro del par `key =>  value` dependiendo de  cómo se defina el valor, éste será formateado de una manera o de otra por ejemplo:

```c#
 var query = (new QueryBuilder())
		  .table("users")
		  .update(new Dictionary<string, string>(){
                {"name", "'juan'"},
                {"id", "1"},
                {"surname", "empleados.surname"}
                })
     	.from("empleados")
		  .where($"id = empleados.user_id");

Console.WriteLine(query);
/*
* Produce el siguiente resultado:
* UPDATE users SET name='juan', id=1, surname=empleado.surname FROM (SELECT name, id FROM roles WHERE baja = 0) AS roles WHERE users.name LIKE '%hector' AND roles.id = users.id
*/
```

En este caso  podemos ver que la asignación se puede hacerse de tres formas diferentes o el string del valor se puede formatear de formas diferentes según la  forma del string.

```c#
{"name", "'juan'"} => SET name='juan' //asume que el dato que se asigna un string
{"id", "1"} => SET id=1 // asume que se esta asignando un valor entero
{"surname", "empleados.surname"} => SET surname=empleados.surname // se asume que se invoca una variable, probablemente desde el FROM
```



## Selección de datos

Las siguientes funciones permiten la selección de datos. A continuación se muestra el algoritmo con el cual QueryBuilder construye las queries de selección.

![select](./Images/select_state.png)



### Campos

#### QueryBuilder.select()
El método `select()` permite determinar los campos que serán seleccionados en la consulta, está carga es incremental con lo cual permite sumar campos según una determinada condición por ejemplo.

```c#
var query = (new QueryBuilder())
		.from("empleados")
        .select("nombre", "surname AS apellido")
        .select("dni");

Console.Writeline(query);
/*
* Produce el siguiente resultado:
* SELECT nombre, surname AS apellido, dni FROM empleados 
*/
```

#### QueryBuilder.distinct()

La función `distinct()` como su nombre lo indica permite obtener los distintos resultados unicamente distintos. La función `distinct()` no recibe parámetros
```c#
var query = (new QueryBuilder())
		.from("empleados")
        .select("apellido")
        .distinct();

Console.Writeline(query);
/*
* Produce el siguiente resultado:
* SELECT DISTINCT apellido FROM empleados
*/
```


### Tablas

#### QueryBuilder.from()
Como se menciona más arriba,  al menos una vez la función from debe ser invocada. Sin embargo no quita que la misma sea invocada más de una vez.
A diferencia de la función `select()` que puede recibir **n*** parámetros, está solo recibe un sólo parámetro que es el nombre de la tabla y tiene un parámetro opcional para el alias. A continuación se detalla su invocación:

```c#
var query = (new QueryBuilder())
		.from("empleados")
        .from("clients", "clientes");

Console.Writeline(query);
/*
* Produce el siguiente resultado:
* SELECT * FROM empleados, clients AS clientes
```

Nótese que si la función `select()` no es invocada, se supone que la selección es del total de campos y el * se coloca forma automática.

#### QueryBuilder.join()
La función `join()` nos permite el merge de dos o mas tablas, dependiendo de la cantidad de veces que se invoque la funcion. La definición de la función `join()` es como se muestra a continuación.
`join (string table, string condition, string method = "inner")`
Como se observa, la función recibe dos parámetros necesarios y un tercer parámetro opcional dependiendo los requerimientos del usuario.
A diferencia de otras funciones cuyo orden no son importantes para la creación de una sentencia **SQL**, la función `join()` si afecta según el orden de su invocación ya que el **JOIN** se hará siempre con la última **tabla** agregada mediante la función `from()`.

Ejemplo:
A continuación se muestra como funciona a través de vincular tres tablas y cuál es su salida correspondiente:

```c#
var query = (new QueryBuilder())
			.from("empleados")
			.from("personas")
			.join("ciudades", "personas.ciudad_id = ciudades.id");


Console.Writeline(query);
/*
* Produce el siguiente resultado:
* SELECT * FROM empleados, personas INNER JOIN ciudades ON personas.id = ciudades.id
*/

var query2 = (new QueryBuilder())
		.from("empleados")
		.join("personas", "empleados.id = personas.id")
		.join("ciudades" , "personas.ciudad_id = ciudades.id", "left");


Console.Writeline(query2);
/*
* Produce el siguiente resultado:
* SELECT * FROM empleados INNER JOIN personas ON empleados.id = personas.id LEFT JOIN ciudades ON personas.ciudad_id = ciudades.id
*/
```

Como se puede observar la función siempre hace el **JOIN** con la última tabla agregada mediante la función `from()`. También es posible especificar a través del parámetro opcional el método, el cual acepta alguna de las siguientes cadenas de texto: `inner`, `left`, `right`, `outer`.

#### QueryBuilder.subQuery()
En algunos casos el conjunto de datos a partir de los cuales se va a operar con otras tablas o se van a seleccionar datos no es una tabla en concreto, sino que es el resultado de una query anterior, es decir una **query** Intermedia que ya constituye el resultado de un procesamiento anterior. En **SQL** esto se consigue mediante el anidamiento de sentencias. **QueryBuilder**  permite construir estás subquerys de forma independiente, separando su complejidad y permitiendo reutilizar las mismas en distintas composiciones. 
La función admite un parámetro obligado y un parámetro opcional que es el **alias**.
A continuación se detalla el uso de `subQuery()`:

```c#

 	var sub = (new QueryBuilder())
        .from("marcas")
        .where("estado = 'active'");
        
        var query = (new QueryBuilder())
            .select("nombre", "codigo", "cantidad")
            .from("productos")
            .subQuery(sub, "cam")
            .where("cam.codigo = productos.codigo_id");

            Console.WriteLine(query);

/*
* Produce el siguiente resultado:
* SELECT nombre, codigo, cantidad FROM
* productos , (SELECT * FROM marcas WHERE estado = 'active') AS cam WHERE cam.codigo = productos.codigo_id
*/

```

### Condiciones

Las condiciones son todas aquellas funciones que agregan contenido y en **sql** vendrían a representar aquellas clausulas que se encuentran despues del  `WHERE` . Es importante resaltar aquí la semántica de las funciones ya que para entender su uso es preciso ententer. Las funciones de condición se construyen como se sigue a continuación:

`[operador][negación][clausula]`

El operador puede ser **and** u **or**, sin embargo para el operador **and** se omite su escritura, mientras que para el operador **or** esta se debe colocar en forma explicita. Por ejemplo, para la función  `andLike()` solo se invoca `like()` sin embargo para la condición de **or** se debe invocar `orLike()`.
Al igual que el operador, la afirmación es tácita mientras que la negación debe ser explicita. la negación la escribimos mediante el vocablo **not**. Ejemplo: siguiendo con la función `like()` podemos usar su negación utilizando `notLike()`.
La clasula es especificamente la acción que se desea consultar, por ejemplo si es un **like**, un **in**, o un **where**. *¡aclaración!: (la función where no admite negación)*.


#### QueryBuilder.where()
La función de condiciones `where()` permite filtrar la búsqueda según determinadas condiciones. La función de condición `where()` se puede invocar más de una vez en cuyos casos unirá las condiciones mediante un `AND`, como también permite cargar en una única invocación una gran cantidad de condiciones. A continuación se ilustra su uso:

```c#
var query = (new QueryBuilder())
			.from("empleados")
			.where("edad > 20", "sexo='mujer'")
			.where("antiguedad >= 30");


Console.Writeline(query);
/*
* Produce el siguiente resultado:
* SELECT * FROM empleados WHERE edad > 20 AND sexo = 'mujer' AND antiguedad >= 30
```



#### QueryBuilder.orWhere()
La función `orWhere()`  como su nombre lo indica, permite agregar condiciones separadas por un `OR` .

```c#
var query = (new QueryBuilder())
		.from("empleados")
        .where("edad > 20")
        .orWhere("antiguedad >= 30" , "sexo='mujer'");


Console.Writeline(query);
/*
* Produce el siguiente resultado:
* SELECT * FROM empleados WHERE edad > 20 OR sexo = 'mujer' OR antiguedad >= 30
```

#### QueryBuilder.whereIn()
La función `whereIn()` permite filtrar los resultados dentro de un conjunto de referencia pasado como parámetro.
Esta función es sumamente util, se puede pasar el conjunto de referencia enumerando cada uno de los elementos como parámetros de la función o bien este método tiene además una sobre carga en la cual se le puede pasar una instancia de **QueryBuilder**, es decir una **subQuery**.

vemos la invocación de este método y la salida correspondiente:
```c#

        var query = (new QueryBuilder())
        .select("nombre", "apellido", "dni")
        .from("choferes")
        .where("sexo = 'varon'")
        .whereIn("camion_id", "12", "23", "35");

/* Produce el siguiente resultado:
* SELECT nombre, apellido, dni FROM choferes WHERE sexo = 'varon' AND camion_id IN (12, 23, 35)
*/
```

pasandole como parámetro una instancia de **QueryBuilder**:
```c#

        var subquery = (new QueryBuilder())
        .select("id")
		.from("camiones")
		.where("marca = 'scania'");

        var query = (new QueryBuilder())
        .select("nombre", "apellido", "dni")
        .from("choferes")
        .where("sexo = 'varon'")
        .whereIn("camion_id", subquery);

/* Produce el siguiente resultado:
* SELECT nombre, apellido, dni FROM choferes WHERE sexo = 'varon' AND camion_id IN ((SELECT id FROM camiones WHERE marca = 'scania'))
*/
```

#### QueryBuilder.whereNotIn()
El uso de la función `whereNotIn()` es similar a la función anterior, con la diferencia que se utiliza para excluir los resultados del conjunto de referencia utilizado como filtro. Esta función también posee sobrecarga de método y puede recibir como parámetro una instancia de **QueryBuilder**.

uso de la función:
```c#

        var query = (new QueryBuilder())
        .select("nombre", "apellido", "dni")
        .from("choferes")
        .where("sexo = 'varon'")
        .whereNotIn("camion_id", "12", "23", "35");

/* Produce el siguiente resultado:
* SELECT nombre, apellido, dni FROM choferes WHERE sexo = 'varon' AND camion_id NOT IN (12, 23, 35)
*/
```

#### QueryBuilder.like()

La función `like()`  se usa en como una **función de condición** para buscar un patrón específico en una columna. Esta función 
La función recibe dos parámetros necesarios y un tercer parámetro opcional.
Parametros:

- **field**: Es la columna sobre la cual se desea buscar un patrón.
- **match**: Es precisamente el patrón que se desea buscar.
- **wilcard**: Es uno de los siguientes tres strings: `before` | `after` | `both` que hace referencia a antes, despues o ambos. En el caso de que no se especifique, siempre dejará el comodín `%` en ambos lados del match.

Veamos con ejemplo:

```c#
var query = (new QueryBuilder())
		.from("empleados")
        .like("apellido", "Gomez", "after")
		.like("nombre", "Alber");

Console.Writeline(query);
/*
* Produce el siguiente resultado:
* SELECT * FROM empleados WHERE apellido LIKE 'Gomez%' AND nombre LIKE '%Alber%'
*/
```

#### QueryBuilder.orLike() , QueryBuilder.NotLike(), QueryBuilder.orNotLike()

Como se menciona en la introducción a la sección, es posible utilizar el operador **or** y la negación para la clausula **like**. Los parámetros siguen siendo los mismos y en caso de omitir el parámetro `wilcard` este asume el valor "both".
A continuación se expone un ejemplo aunque el mismo dificil

```c#
var query = (new QueryBuilder())
		.from("usuarios")
        .like("nombre", "Marti", "after")
		.orLike("apellido", "meri")
        .notLike("rol", "adm", "both")
        .orNotLike("sexo", "jer", "before");

Console.Writeline(query);
/*
* Produce el siguiente resultado:
* SELECT * FROM usuarios WHERE nombre LIKE 'Marti%' AND rol NOT LIKE '%adm%' OR apellido LIKE '%meri%' OR sexo NOT LIKE '%jer'
*/
```

### Ordenamiento

#### QueryBuilder.orderBy()
La función `orderBy()` permite el ordenamiento de los resultados. Está función no tiene restricciones de parámetros, es decir se le puede pasar la cantidad de condiciones de ordenamiento que sean necesarias. Tampoco tiene restricciones de invocación cómo la función `join()` y se puede invocar en cualquier momento.

Detalle del uso de `orderBy()`:

```c#
var query = (new QueryBuilder())
		.from("empleados")
        .orderBy("id ASC");

Console.Writeline(query);
/*
* Produce el siguiente resultado:
* SELECT * FROM empleados ORDER BY id ASC
*/
```

### Paginación
#### QueryBuilder.paginate()
La Paginación permite el paginado de los resultados, es decir poder obtener como resultado un subgrupo del total de los resultados. En combinación con `orderBy()` permite obtener un control total sobre la organización de la información ya que se pueden ordenar los datos por algún campo y luego con la Paginación se pueden segmentar los resultados.
La función recibe dos parámetros `page` y `limit` necesariamente para especificar cómo serán segmentados los resultados.
- **page** para indicar el número de página o segmento que se desea obtener.
- **limit** para informar que tamaño debería tener cada segmento.

A continuación se muestra su uso:

```c#
var query = (new QueryBuilder())
		.from("empleados")
        . orderBy("id DESC")
        .paginate(3, 10);

Console.Writeline(query);
/*
* Produce el siguiente resultado:
* SELECT * FROM empleados ORDER BY id DESC LIMIT 10 OFFSET 20
* SELECT SKIP 30 FIRST 30 * FROM empleados ORDER BY id DESC
*/
```


### Union
La union es la posibilidad de unir resultados de dos o más consultas en un solo conjunto de datos. Para ello las dos consultas a unir deben tener columnas con el mismo nombre y el mismo tipos de datos. Esto hará que las columnas se fusionen en un solo conjunto de datos.
Ejemplo:

```c#
var query = (new QueryBuilder())
		.from("customers")
        . select("city");

var query2 = (new QueryBuilder())
.from("suppliers")
.select("city");

query.union(query2);

Console.Writeline(query);
/*
* Produce el siguiente resultado:
* SELECT * FROM (SELECT city FROM customers)
* UNION
* SELECT * FROM (SELECT city FROM suppliers)
*/
```

## Licencia

Copyright (C) 2023.

- Héctor Orlando,
  - [![linkedin](https://img.shields.io/badge/GitHub--0a66c2?style=social&logo=GitHub)](https://github.com/hectorcrispens)  [![linkedin](https://img.shields.io/badge/GitHub--FC6D26?style=social&logo=GitLab)](https://gitlab.com/hector.or.cr) 
  - [![linkedin](https://img.shields.io/badge/Gmail--0a66c2?style=social&logo=Gmail)](mailto:hector.or.cr@gmail.com) [![linkedin](https://img.shields.io/badge/LinkedIn--0a66c2?style=social&logo=linkedin)](https://www.linkedin.com/in/hector-orlando-25124a18a/) 



### GNU General Public License

Este programa es software gratuito: puedes redistribuirlo y/o  modificar bajo los términos de la Licencia Pública General GNU tal como  se publicó por la Free Software Foundation, ya sea la versión 3 de la  Licencia, o cualquier versión posterior.

Este programa se distribuye con la esperanza de que sea de utilidad,  pero SIN NINGUNA GARANTÍA; sin siquiera la garantía implícita de  COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO PARTICULAR. Ver el Licencia  pública general GNU para más detalles.

Debería haber recibido una copia de la Licencia Pública General GNU junto con este programa, en LICENSE.md o https://www.gnu.org/licenses/gpl-3.0.html.en.
