using System.Collections.Generic;
using System;
namespace Querybuilder
{
    public class Role
    {
        public int id { get; set; }
        public string name { get; set; }
        public int active { get; set; }

        public Role(int id, string name, int active)
        {
            this.id = id;
            this.name = name;
            this.active = active;
        }
    
    }
}