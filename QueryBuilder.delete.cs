using System;
using System.Collections.Generic;
using System.Linq;

namespace Querybuilder;

public partial class QueryBuilder 
{     
   



        public QueryBuilder delete()
        {
            this.operator_selected = this.OPERATORS[3];
            return this;
        }

        public QueryBuilder delete(string name)
        {
             this.table_name = name;
             this.operator_selected = this.OPERATORS[3];
            return this;
        }

        partial void resolve_delete()
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(this.table_name)){
                string filter = this.conditions.Length == 0 ? "" : " WHERE " + string.Join(" ", this.conditions);
              
              result = 
                $"DELETE FROM {table_name}"
              + filter;

              this.final_string = result;
              return;
            }
                     
	        return;
        }


}