using System;
using System.Collections.Generic;
using System.Linq;

namespace Querybuilder;

public partial class QueryBuilder 
{             
    /* CAMPOS */

    /* Selección de campos, en el caso de que no se ingrese se utiliza el "*" */
    public QueryBuilder select(params string[] args)
    {
        this.operator_selected = this.OPERATORS[0];

        this.fields = this.fields.Concat(args).ToArray();
        return this;
    }

    public QueryBuilder distinct()
    {
        this.str_distinct = " DISTINCT";

        return this;
    }

    /* AND HAVINGS */
    public QueryBuilder having(params string[] args)
    {	
    	string q = this.prehave ? "AND " : "";
        q += string.Join("AND ", args);
        this.have = this.have.Concat(new string[]{q}).ToArray();
        this.prehave = true;
        return this;
    }

    /* OR HAVINGS */
    public QueryBuilder orHaving(params string[] args)
    {	
        string q = this.prehave ? "OR " : "";
    	q += string.Join("OR ", args);
        this.have = this.have.Concat(new string[]{q}).ToArray();
        this.prehave = true;
        return this;
    }
   

    /* PAGINACION */
    public QueryBuilder paginate(int page, int limit = 0)
    {
        this.page = (page - 1) * limit;
        this.limit = limit;

        return this;
    }

    /* ORDERING */
    public QueryBuilder orderBy(params string[] args)
    {
        this.order = this.order.Concat(args).ToArray();
        return this;
    }

    /* GROUPING */
    public QueryBuilder groupBy(params string[] args)
    {
	this.groupby = this.groupby.Concat(args).ToArray();
	return this;
    }
  

    partial void resolve_select()
    {
        if (this.tables.Length == 0) return;
        // string skip = this.page == 0 ? "" : $" SKIP {page.ToString()}";
        // string first = this.limit == 0 ? "" : $" FIRST {limit.ToString()}";

        string offest = this.page == 0 ? "" : $" OFFSET {this.page.ToString()}";
        string limit = this.limit == 0 ? "" : $" LIMIT {this.limit.ToString()}";

	
        string selection = this.fields.Length == 0 ? " *" : " " + String.Join(", ", this.fields);

	    string sources = this.tables.Length == 0 ? "" : " FROM " + String.Join(", ", this.tables);

	    string filter = this.conditions.Length == 0 ? "" : " WHERE " + string.Join(" ", this.conditions);
	
	    string group_by = this.groupby.Length == 0 ? "" : " GROUP BY " + String.Join(", ", this.groupby);
	
        string order_by = this.order.Length == 0 ? "" : " ORDER BY " + String.Join(", ", this.order);

        string having = this.have.Length == 0 ? "" : " HAVING " + String.Join(" ", this.have);
	


	string pre_process = "SELECT"
	    + this.str_distinct
	    //+ skip + first
	    + selection
	    + sources
	    + filter
	    + group_by
        + having
	    + order_by
        + limit + offest;
				
	string final = union_query == null ? pre_process:
	    $"SELECT * FROM ({pre_process}) UNION SELECT * FROM ({this.union_query.ToString()})";

        this.final_string= final;
        return;
    }

}
