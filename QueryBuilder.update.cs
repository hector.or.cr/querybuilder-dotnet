using System;
using System.Collections.Generic;
using System.Linq;

namespace Querybuilder;

public partial class QueryBuilder 
{     
   



        public QueryBuilder update(Dictionary<string, string> rows)
        {
            this.operator_selected = this.OPERATORS[2];
            this.update_dataset = rows;
            return this;
        }

         public QueryBuilder update(object data)
        {
            Dictionary<string, string> rows = data.GetType().GetProperties()
            .ToDictionary(x => x.Name, x => x.GetValue(data)?.ToString() ?? "");

            this.operator_selected = this.OPERATORS[2];
            this.update_dataset = rows;

          return this;
        }



        partial void resolve_update()
        {
            string result = string.Empty;
            if (this.update_dataset.Count > 0 && !string.IsNullOrEmpty(this.table_name)){
                string s = string.Join(", ", this.update_dataset.Select(x => x.Key + "=" + x.Value).ToArray());
                string set = $" SET {s}";
                string sources = this.tables.Length == 0 ? "" : " FROM " + String.Join(", ", this.tables);
                string filter = this.conditions.Length == 0 ? "" : " WHERE " + string.Join(" ", this.conditions);
              
              result = 
                $"UPDATE {table_name}"
              + set
              + sources
              + filter;

              this.final_string = result;
              return;
            }
                     
	        return;
        }


}