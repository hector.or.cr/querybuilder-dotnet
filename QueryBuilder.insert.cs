using System;
using System.Collections.Generic;
using System.Linq;

namespace Querybuilder;

public partial class QueryBuilder 
{     
   



        public QueryBuilder columns(params string[] args)
        {
            this.columns_filter = this.columns_filter.Concat(args).ToArray();
            this.columns_filter = this.columns_filter.Distinct().ToArray();
            return this;

        }
        public QueryBuilder insert(Dictionary<string, string> rows)
        {
            this.operator_selected = this.OPERATORS[1];
            this.dataset = this.dataset.Concat(new Dictionary<string, string>[]{rows}).ToArray();
            return this;
        }
        public QueryBuilder insert(Dictionary<string, string>[] rows)
        {
            this.operator_selected = this.OPERATORS[1];
            this.dataset = this.dataset.Concat(rows).ToArray();
            return this;
        }

         public QueryBuilder insert(object data)
        {
            Dictionary<string, string> rows = data.GetType().GetProperties()
            .ToDictionary(x => x.Name, x => x.GetValue(data)?.ToString() ?? "");

            this.operator_selected = this.OPERATORS[1];
            this.dataset = this.dataset.Concat(new Dictionary<string, string>[]{rows}).ToArray();

          return this;
        }



        partial void resolve_insert()
        {
            string result = string.Empty;
            if (this.columns_filter.Length > 0 && this.dataset.Length > 0 && !string.IsNullOrEmpty(this.table_name)){
              result = $"INSERT INTO {table_name} ({string.Join(", ", columns_filter)}) VALUES ";
            string[] result_values = {};

            foreach(Dictionary<string, string> row in dataset)
            {
                string q = "(";
                string[] values = columns_filter.Select(v => {return row[v];}).ToArray();
                q+=string.Join(", ", values) + ")";
                result_values = result_values.Concat(new string[]{q}).ToArray();
            }
             result+=string.Join(", ", result_values);
            }
            this.final_string = result;         
	        return;
        }


}