using System;
using System.Collections.Generic;
using System.Linq;

namespace Querybuilder;

public partial class QueryBuilder 
{         

    private string[] fields = {};
    private string[] conditions = {};
    private bool connector = false;
    private string[] tables = {};
    private string[] order = {};
    private bool prehave = false;
    private string[] have = {};
    private string[] groupby = {};
    private int page = 0;
    private int limit = 0;
    private string str_distinct = "";
    

    private string[] JOIN_METHODS = {"inner", "outer", "left", "right"};
    private string[] OPERATORS = {"select", "insert", "update", "delete", "truncate"};
    private string operator_selected = "select";

    private QueryBuilder union_query = null;

    private string table_name = "";

    /* INSERT */  
    private Dictionary<string, string>[] dataset = {};
    private string[] columns_filter = {};

    /* UPDATE */
    private Dictionary<string, string> update_dataset = new Dictionary<string, string>();

    private string final_string = "";

    public override string ToString()
    {

        switch(this.operator_selected)
        {
            case "select":
            this.resolve_select();
            break;
            case "insert":
            this.resolve_insert();
            break;
            case "update":
            this.resolve_update();
            break;
            case "delete":
            this.resolve_delete();
            break;
           
        }
	    return this.final_string;
    }
    partial void resolve_select();
    partial void resolve_insert();
    partial void resolve_update();
    partial void resolve_delete();

    /* RESETEO */
   public QueryBuilder reset()
    {

	 this.fields = new string[]{};
	 this.conditions = new string[]{};
	 this.connector = false;
	 this.tables = new string[]{};
	 this.order = new string[]{};
	 this.groupby = new string[]{};
     this.have = new string[]{};
	 this.page = 0;
	 this.limit = 0;
	 this.str_distinct = "";
	 this.union_query = null;
     this.table_name = "";
     this.operator_selected = OPERATORS[0];
     this.dataset =  new Dictionary<string, string>[]{};
     this.columns_filter = new string[]{};
     this.union_query = null;
     this.update_dataset = new Dictionary<string, string>();

	 return this;
    }
    
    /* TABLAS */
    public QueryBuilder table(string name)
    {
            this.table_name = name;
            return this;
    }

    
    /* ORIGENES DE DATOS */

    /* Permite agregar tablas al from, solo permite agregar de a una. Opcional se puede agregar un alias*/
    public QueryBuilder from(string tables, string alias = "")
    {
        if (string.IsNullOrEmpty(alias)) {
            this.tables = this.tables.Concat(new string[] {tables}).ToArray();
        } else {
            this.tables = this.tables.Concat(new string[] {$"{tables} AS {alias}"}).ToArray();
        }
        return this;
    }
    
    /* Permite hacer un join entre dos tablas, siempre hace join con la ultima tabla. Opcional es el method */
    public QueryBuilder join (string tables, string condition, string method = "inner")
    {
        
        method = this.JOIN_METHODS.Contains(method.ToLower()) ? method : this.JOIN_METHODS[0];
        string q = $" {method.ToUpper()} JOIN {tables} ON {condition} ";
        this.tables[this.tables.Length - 1] += q;
        
        return this;
    }
    
    /* Agrega un conjunto de datos, al from a partir de otro QueryBuilder */
      public QueryBuilder subQuery(QueryBuilder sbq, string alias = "")
    {
        if (string.IsNullOrEmpty(alias)) {
            this.tables = this.tables.Concat(new string[] {$"({sbq.ToString()})"}).ToArray();
        } else {
            this.tables = this.tables.Concat(new string[] {$"({sbq.ToString()}) AS {alias}"}).ToArray();
        }
        return this;
    }

  

    /* AND CONDICIONES */

    /* where */
    public QueryBuilder where(params string[] args)
    {	
    	string q = string.Join(" AND ", args);
        return this.process_where(q, "AND");
    }

    /* in */
    public QueryBuilder whereIn(string field , params string[] args)
    {
    	
        string q = $"{field} IN (" + String.Join(", ", args) + ")";
        return this.process_where(q, "AND");
    }

    public QueryBuilder whereIn(string field ,QueryBuilder subquery)
    {	
        string q = $"{field} IN (" + subquery.ToString() + ")";
        return this.process_where(q, "AND");
    }

    /* not in */
    public QueryBuilder whereNotIn(string field , params string[] args)
    {
    	
        string q = $"{field} NOT IN (" + String.Join(", ", args) + ")";
        return this.process_where(q, "AND");
    }

    public QueryBuilder whereNotIn(string field ,QueryBuilder subquery)
    {
        string q = $"{field} NOT IN (" + subquery.ToString() + ")";
        return this.process_where(q, "AND");
    }

    /* like */
    public QueryBuilder like(string field , string match, string wilcard = "both")
    { 
        return this.process_like(field, match, wilcard, "LIKE");
    }

    public QueryBuilder notLike(string field , string match, string wilcard = "both")
    { 
        return this.process_like(field, match, wilcard, "NOT LIKE");
    }
    
 
    
    /* OR CONDICIONES */
    
    /* or where */
    public QueryBuilder orWhere(params string[] args)
    {
	string q = string.Join(" OR ", args);
        return this.process_where(q, "OR");
    }


    /* or where in */
    public QueryBuilder orWhereIn(string field , params string[] args)
    {
        string q = $"{field} IN (" + String.Join(", ", args) + ")";
        return this.process_where(q, "OR");
    }

    public QueryBuilder orWhereIn(string field ,QueryBuilder subquery)
    {
        string q = $"{field} IN ((" + subquery.ToString() + "))";
        return this.process_where(q, "OR");
    }

    /* or where not in */
    public QueryBuilder orWhereNotIn(string field , params string[] args)
    {
        string q = $"{field} NOT IN (" + String.Join(", ", args) + ")";
        return this.process_where(q, "OR");
    }

    public QueryBuilder orWhereNotIn(string field ,QueryBuilder subquery)
    {
        string q = $"{field} NOT IN ((" + subquery.ToString() + "))";
        return this.process_where(q, "OR");
    }

     /* or like */
    public QueryBuilder orLike(string field , string match, string wilcard = "both")
    { 
        return this.process_like(field, match, wilcard, "LIKE");
    }

    public QueryBuilder orNotLike(string field , string match, string wilcard = "both")
    { 
        return this.process_like(field, match, wilcard, "NOT LIKE");
    }

    public QueryBuilder groupStart()
    {
	string q = "(";
	this.process_where(q, "AND");
	this.connector = false;
	return this;
    }

    public QueryBuilder orGroupStart()
    {
	string q = "(";
	this.process_where(q, "OR");
	this.connector = false;
	return this;
    }

    public QueryBuilder groupEnd()
    {
	this.conditions = this.conditions.Concat(new string[]{")"}).ToArray();
	this.connector = true;
	return this;
    }


    /* OPERACIONES DE CONJUNTO */
    public QueryBuilder union(QueryBuilder query)
    {
        this.union_query = query;
        return this;
    }
    
    
    
    /* METODOS PRIVADOS */
    
    private QueryBuilder process_where(string q, string pre)
    {
    	q = this.connector ? $"{pre} {q}" : q;
	    this.conditions = this.conditions.Concat(new string[]{q}).ToArray();
	    this.connector=true;
    	return this;
    }

    private QueryBuilder process_like(string field , string match, string wilcard, string op)
    { 
        String pre = this.connector ? "AND " : "";
        string q = $"{pre}{field} {op} ";
        switch (wilcard)
        {
            case "before":
            q+=$"'%{match}'";
            break;
            case "after":
            q+=$"'{match}%'";
            break;
            default:
            q+=$"'%{match}%'";
            break;
        }
             this.conditions = this.conditions.Concat(new string[]{q}).ToArray();
	     this.connector = true;
        return this;
    }

}
